/*****************************************************************************************************************************
 * @file        Fls_Types.h                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.08.2018 10:16:01                                                                                          *
 * @brief       Implementation of module global datatypes from the "Fls" module.                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLS_TYPES_H
#define __FLS_TYPES_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Fls
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the Fls
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  FLS_E_OK = 0,
  FLS_E_NOT_OK = 1,
  FLS_E_PENDING = 2,
  FLS_E_NOT_IMPLEMENTED = 3,
  FLS_E_GENERIC_PROGRAMMING_FAILURE = 4,
  FLS_E_SKIPPED = 5,

  FLS_E_INVALID_ARGUMENT = 100
}Fls_ReturnType;

/**
 * @brief           Available application return values of the Fls
 * @details         Redefinition of the Fls_ReturnType as error type.
 */
typedef Fls_ReturnType Fls_ErrorType;

/**
 * @brief           Flash sector configuration type
 */
typedef struct
{
  uint32 flsSectorStartAdr;
  uint32 flsSectorEndAdr;
  uint32 flsId;
}Fls_SectorCfgType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLS_TYPES_H*/

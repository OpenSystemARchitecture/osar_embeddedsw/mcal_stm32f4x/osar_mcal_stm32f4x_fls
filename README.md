# OSAR_MCAL_STM32F4X_Fls

## General
The Fls Modul is the abstraction of the flash hardware interface. It could be used to erase and program the flash memory.

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - Fls releases](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/MCAL_STM32F4X/OSAR_Fls/)
- [OSAR - Documents](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/)
- [OSAR - Hyperspace](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/)
  - Main-Tool to setup an OSAR Embedded Software Project.
- [OSAR - Tools](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/)
echo off
cd Libs

echo ">>>>> Download latest OSAR Source File Library <<<<<"
wget http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_SourceFileLib/latest/ -r --no-parent -nH --cut-dirs=7 --proxy=off -R "index.html*"
::RENAME .\doxygen.zip SourceFileLibDoxygen.zip
DEL .\doxygen.zip

echo ">>>>> Download latest OSAR Resource Library <<<<<"
wget http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Resources/latest/ -r --no-parent -nH --cut-dirs=7 --proxy=off -R "index.html*"
::RENAME .\doxygen.zip ResourceLibDoxygen.zip
DEL .\doxygen.zip

echo ">>>>> Download latest OSAR Rte-Lib Library <<<<<"
wget http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Rte/latest/01_Generator/RteLib.dll -r --no-parent -nH --cut-dirs=9 --proxy=off -R "index.html*"
wget http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Rte/latest/01_Generator/RteLib.pdb -r --no-parent -nH --cut-dirs=9 --proxy=off -R "index.html*"
wget http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Rte/latest/05_Doc/TechnicalReference_RteLib.pdf -r --no-parent -nH --cut-dirs=9 --proxy=off -R "index.html*"

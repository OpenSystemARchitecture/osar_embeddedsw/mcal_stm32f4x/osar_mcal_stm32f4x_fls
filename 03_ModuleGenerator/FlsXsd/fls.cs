﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using OsarResources.XML;
using OsarResources.Generic;

namespace FlsXsd
{
  public enum FlsProgrammingVoltageRange
  {
    FLASH_VOLTAGE_RANGE_1,
    FLASH_VOLTAGE_RANGE_2,
    FLASH_VOLTAGE_RANGE_3,
    FLASH_VOLTAGE_RANGE_4
  }

  public struct FlsFlashSectorCfg
  {
    public String flsSectorStartAddress;
    public String flsSectorEndAddress;
    public UInt16 flsSectorId;
  }

  public class FlsXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public FlsProgrammingVoltageRange flsUsedProgrammingVoltage;
    public List<FlsFlashSectorCfg> flsSectorConfigList;
  }
}

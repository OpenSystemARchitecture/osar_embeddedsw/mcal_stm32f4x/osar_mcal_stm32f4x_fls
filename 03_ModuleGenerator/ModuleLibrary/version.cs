/******************************************************************************
 * @file      version.cs
 * @author    OSAR S.Reinemuth
 * @proj      ModuleLibrary
 * @date      Saturday, January 2, 2021
 * @version   Application v. 1.2.0.1
 * @version   Generator   v. 1.2.5.1
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.2.0.1")]

namespace ModuleLibrary
{
  static public class ModuleLibraryVersionClass
  {
    public static int major { get; set; }  //Version of the program
    public static int minor { get; set; }  //Sub version of the program
    public static int patch { get; set; }  //Debug patch of the program
    public static int build { get; set; }  //Count program builds

    static ModuleLibraryVersionClass()
    {
			major = 1;
			minor = 2;
			patch = 0;
			build = 1;
    }

    public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
  }
}
//!< @version 0.0.1	->	Initial Project Setup
//!< @version 1.1.0	->	Adding inital version of the new module library
//!< @version 1.1.1	->	Update dependency libraries
//!< @version 1.2.0	->	Update to RteLib v1.0.0

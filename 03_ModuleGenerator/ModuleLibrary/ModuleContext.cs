﻿/*****************************************************************************************************************************
 * @file        ModuleContext.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.04.2019                                                                                                   *
 * @brief       Implementation of the central module DLL interface class "ModuleContext"                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generator;
using System.Windows.Controls;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary
{
  /// <summary>
  /// Public Interface function to access the DLL internal modules and data structures
  /// </summary>
  public class ModuleContext : OsarModuleContextInterface
  {
    string pathToXmlFile = "";
    string pathToModuleBaseFolder = "";
    private static volatile ModuleContext instance = null;

    // Helper object for a safe thread synchronization
    private static object _lock = new object();

    #region Public Module Context Interfaces
    /// <summary>
    /// Constructor
    /// </summary>
    public ModuleContext()
    {
      //Nothing to do
    }

    /// <summary>
    /// Interface to get and instance of the moduleContext
    /// Implementation of an Singleton pattern
    /// </summary>
    /// <returns> Returns an instance with all public interfaces </returns>
    public static OsarModuleContextInterface GetInstance()
    {
      if (null == instance)
      {
        lock (_lock)
        {
          if (null == instance)
          {
            instance = new ModuleContext();
          }
        }
      }
      return instance;
    }

    /// <summary>
    /// Interface function to get the active view with active version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Return of view object</returns>
    public UserControl GetView(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      return SelectVersionView(DetectActiveVersion());
    }

    /// <summary>
    /// Interface function to get the active view with specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="viewVersion">Version of return view object</param>
    /// <returns>Return of view object</returns>
    public UserControl GetView(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion viewVersion)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      return SelectVersionView(viewVersion);
    }

    /// <summary>
    /// Interface function to get the active view model with active version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Return of view model object</returns>
    public OsarModuleGeneratorInterface GetViewModel(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      return SelectVersionViewModel(DetectActiveVersion());
    }

    /// <summary>
    /// Interface function to get the active view model with specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="viewVersion">Version of return view model object</param>
    /// <returns>Return of view object</returns>
    public OsarModuleGeneratorInterface GetViewModel(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion viewVersion)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      return SelectVersionViewModel(viewVersion);
    }

    /// <summary>
    /// Interface function to get the active configuration file version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Active file version</returns>
    public XmlFileVersion GetActiveVersion(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      return SelectVersionViewModel(DetectActiveVersion()).XmlFileVersion;
    }

    /// <summary>
    /// Interface function to get a list with available versions
    /// </summary>
    /// <returns>List with available versions</returns>
    public List<XmlFileVersion> GetAvailableVersions()
    {
      List<XmlFileVersion> xmlFileVersions = new List<XmlFileVersion>();
      XmlFileVersion xmlFileVersion = new XmlFileVersion();

      /* v.<m.m.p.> */
      xmlFileVersion.MajorVersion = 1;
      xmlFileVersion.MinorVersion = 0;
      xmlFileVersion.PatchVersion = 0;
      xmlFileVersions.Add(xmlFileVersion);

      return xmlFileVersions;
    }

    /// <summary>
    /// Interface to convert the active configuration file to a specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="fileVersion"></param>
    public void ConvertCfgFileToSpecificVersion(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion fileVersion)
    {
      pathToXmlFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      XmlFileVersion activeVersion = DetectActiveVersion();

      if (true == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(activeVersion, fileVersion))
      {
        //Nothing to do
      }
      else if (true == OsarResources.Generic.OsarGenericHelper.XmlFileVersionGreater(activeVersion, fileVersion))
      {
        DowngradeCfgFileToSpecificVersion(fileVersion);
      }
      else
      {
        UpgradeCfgFileToSpecificVersion(fileVersion);
      }
    }
    #endregion


    #region Private Interfaces
    #endregion

    #region Private helper functions
    /// <summary>
    /// Helper function to detect the active version of the configuration file
    /// </summary>
    /// <returns>Version of active configuration file</returns>
    private XmlFileVersion DetectActiveVersion()
    {
      Versions.v_1_0_0.ViewModels.Module_ViewModel viewModel = new Versions.v_1_0_0.ViewModels.Module_ViewModel(pathToXmlFile, pathToModuleBaseFolder);
      return viewModel.XmlFileVersion;
    }

    /// <summary>
    /// Selector of an specific view version
    /// </summary>
    /// <param name="objVersion"></param>
    /// <returns></returns>
    private UserControl SelectVersionView(XmlFileVersion objVersion)
    {
      UserControl retVal = null;
      switch (OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(objVersion))
      {
        /* Version <NewestVersion>  >> Example v1.1.1 == 0x0000000100010001*/
        case 0x0000000100000000:
        {
          retVal = new Versions.v_1_0_0.Views.ModuleCfgView(pathToXmlFile, pathToModuleBaseFolder);
        }
        break;

        /* Version unknown */
        default:
        {
          throw new ArgumentOutOfRangeException("Configuration file version unknown >> v." + objVersion.MajorVersion.ToString() +
            "." + objVersion.MinorVersion.ToString() +
            "." + objVersion.PatchVersion.ToString());
        }
        break;
      }

      return retVal;
    }

    /// <summary>
    /// Selector of an specific view model version
    /// </summary>
    /// <param name="objVersion"></param>
    /// <returns></returns>
    private OsarModuleGeneratorInterface SelectVersionViewModel(XmlFileVersion objVersion)
    {
      OsarModuleGeneratorInterface retVal = null;
      switch (OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(objVersion))
      {
        /* Version <NewestVersion>  >> Example v1.1.1 == 0x0000000100010001*/
        case 0x0000000100000000:
        {
          retVal = new Versions.v_1_0_0.ViewModels.Module_ViewModel(pathToXmlFile, pathToModuleBaseFolder);
        }
        break;

        /* Version unknown */
        default:
        {
          throw new ArgumentOutOfRangeException("Configuration file version unknown >> v." + objVersion.MajorVersion.ToString() +
            "." + objVersion.MinorVersion.ToString() +
            "." + objVersion.PatchVersion.ToString());
        }
        break;
      }

      return retVal;
    }

    /// <summary>
    /// Interface to upgrade the active configuration file to an specific version
    /// </summary>
    /// <param name="fileVersion"></param>
    private void UpgradeCfgFileToSpecificVersion(XmlFileVersion fileVersion)
    {
      UInt64 activeVersion = OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(DetectActiveVersion());
      UInt64 requestedVersion = OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(fileVersion);
      while (activeVersion != requestedVersion)
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Interface to downgrade the active configuration file to an specific version
    /// </summary>
    /// <param name="fileVersion"></param>
    private void DowngradeCfgFileToSpecificVersion(XmlFileVersion fileVersion)
    {
      UInt64 activeVersion = OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(DetectActiveVersion());
      UInt64 requestedVersion = OsarResources.Generic.OsarGenericHelper.ConvertXmlFileVersionToU64(fileVersion);
      while (activeVersion != requestedVersion)
      {
        throw new NotImplementedException();
      }
    }


    #endregion
  }
}
/**
 * @}
 */

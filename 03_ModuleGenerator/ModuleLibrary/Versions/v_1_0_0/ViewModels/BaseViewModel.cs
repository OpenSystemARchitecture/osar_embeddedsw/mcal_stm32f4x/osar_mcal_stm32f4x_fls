﻿/*****************************************************************************************************************************
 * @file        BaseViewModel.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Basic View Mode Interfaces                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// A base view model that fires property changed events as needed
  /// </summary>
  [AddINotifyPropertyChangedInterface]
  public class BaseViewModel : INotifyPropertyChanged
  {
    static StoreConfiguration storeConfiguration;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="storeCfg"></param>
    internal BaseViewModel(StoreConfiguration storeCfg)
    {
      storeConfiguration = storeCfg;
    }

    /// <summary>
    /// The event that is fired when any child property changes its value
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged = (sender, e) =>
    {
      storeConfiguration();
    };

    /// <summary>
    /// Encapsulation of property changed event
    /// </summary>
    /// <param name="e"></param>
    protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
    {
      if (null != PropertyChanged)
      {
        PropertyChanged(this, e);
      }
    }
  }
}
/**
 * @}
 */

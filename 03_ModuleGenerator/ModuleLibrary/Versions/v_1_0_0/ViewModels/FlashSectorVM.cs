﻿/*****************************************************************************************************************************
 * @file        FlashSectorVM.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        31.12.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Flash sector configuration                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  public class FlashSectorVM : BaseViewModel
  {
    FlsFlashSectorCfg usedFlsSector;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sector"></param>
    /// <param name="storeCfgDel"></param>
    public FlashSectorVM(FlsFlashSectorCfg sector, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedFlsSector = sector;
    }

    /// <summary>
    /// Interface for the complete flash sector
    /// </summary>
    public FlsFlashSectorCfg FlashSector
    {
      get => usedFlsSector;
      set => usedFlsSector = value;
    }

    /// <summary>
    /// Interface for Attribute object usedFlsSector.flsSectorStartAddress
    /// </summary>
    public String FlsSectorStartAddress
    {
      get => usedFlsSector.flsSectorStartAddress;
      set => usedFlsSector.flsSectorStartAddress = value;
    }

    /// <summary>
    /// Interface for Attribute object usedFlsSector.flsSectorEndAddress
    /// </summary>
    public String FlsSectorEndAddress
    {
      get => usedFlsSector.flsSectorEndAddress;
      set => usedFlsSector.flsSectorEndAddress = value;
    }

    /// <summary>
    /// Interface for Attribute object usedFlsSector.flsSectorId
    /// </summary>
    public UInt16 FlsSectorId
    {
      get => usedFlsSector.flsSectorId;
      set => usedFlsSector.flsSectorId = value;
    }
  }
}

﻿/*****************************************************************************************************************************
 * @file        DefaultCfg.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class DefaultCfgGenerator
  {
    private Models.FlsXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be set to default </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(Models.FlsXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      info.AddInfoMsg(DefResources.InfoMsg_StartDefaultCfgCreation + DefResources.CfgFileMajorVersion +
        "." + DefResources.CfgFileMinorVersion + "." + DefResources.CfgFilePatchVersion);
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new Models.FlsXml();
      }

      // Create Configuration File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);

      // Create Det Module Id
      xmlCfg.detModuleID = Convert.ToUInt16(DefResources.DefaultModuleId);

      // Create module Det usage state
      if (DefResources.DefaultModuleUsage == SystemState.STD_ON.ToString())
        xmlCfg.detModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.detModuleUsage = SystemState.STD_OFF;

      info.AddInfoMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Using default configuration of Processor STM32F407VGT.");

      /* Set Data Setup used voltage range */
      xmlCfg.flsUsedProgrammingVoltage = FlsProgrammingVoltageRange.FLASH_VOLTAGE_RANGE_4;

      /* Set Flash sector data for Stm32f407 */
      xmlCfg.flsSectorConfigList = new List<FlsFlashSectorCfg>();
      FlsFlashSectorCfg flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08000000";
      flashSector.flsSectorEndAddress = "0x08003FFF";
      flashSector.flsSectorId = 0;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08004000";
      flashSector.flsSectorEndAddress = "0x08007FFF";
      flashSector.flsSectorId = 1;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08008000";
      flashSector.flsSectorEndAddress = "0x0800BFFF";
      flashSector.flsSectorId = 2;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x0800C000";
      flashSector.flsSectorEndAddress = "0x0800FFFF";
      flashSector.flsSectorId = 3;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08010000";
      flashSector.flsSectorEndAddress = "0x0801FFFF";
      flashSector.flsSectorId = 4;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08020000";
      flashSector.flsSectorEndAddress = "0x0803FFFF";
      flashSector.flsSectorId = 5;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08040000";
      flashSector.flsSectorEndAddress = "0x0805FFFF";
      flashSector.flsSectorId = 6;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08060000";
      flashSector.flsSectorEndAddress = "0x0807FFFF";
      flashSector.flsSectorId = 7;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x08080000";
      flashSector.flsSectorEndAddress = "0x0809FFFF";
      flashSector.flsSectorId = 8;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x080A0000";
      flashSector.flsSectorEndAddress = "0x080BFFFF";
      flashSector.flsSectorId = 9;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x080C0000";
      flashSector.flsSectorEndAddress = "0x080DFFFF";
      flashSector.flsSectorId = 10;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      flashSector = new FlsFlashSectorCfg();
      flashSector.flsSectorStartAddress = "0x080E0000";
      flashSector.flsSectorEndAddress = "0x080FFFFF";
      flashSector.flsSectorId = 11;
      xmlCfg.flsSectorConfigList.Add(flashSector);

      info.AddLogMsg(DefResources.LogMsg_CreationDone);

      return info;
    }
  }
}
/**
 * @}
 */

/*****************************************************************************************************************************
 * @file        Fls.c                                                                                                        *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        09.12.2020 15:57:19                                                                                          *
 * @brief       Generated Rte Module Interface file: Fls.c                                                                   *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Fls 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Fls.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Fls.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"

/*----------------------------------------------- External defined variables -----------------------------------------------*/
extern Fls_SectorCfgType flsSectorCfgList[];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Fls_START_SEC_CODE
#include "Fls_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            aa4c8440ec1341f3ba4fabd4b637ef05
 */
VAR(void) Fls_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 0ms
 * @uuid            
 */
VAR(void) Fls_( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Fls_STOP_SEC_CODE
#include "Fls_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define Fls_START_SEC_CODE
#include "Fls_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Fls_InitMemory( void )
{

}

/**
 * @brief           Lock the Flash module so no requests would be accepted
 * @param[in]       None
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK
 */
Fls_ReturnType Fls_LockFlash(void)
{
  /* Lock flash module */
  HAL_FLASH_Lock();

  return FLS_E_OK;
}

/**
 * @brief           Unlock the Flash module so requests would be accepted
 * @param[in]       None
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK
 *                  > FLS_E_NOT_OK
 */
Fls_ReturnType Fls_UnLockFlash(void)
{
  HAL_StatusTypeDef halRetVal;
  Fls_ReturnType retVal;

  /* Unlock flash module */
  halRetVal = HAL_FLASH_Unlock();

  if(HAL_OK == halRetVal)
  {
    retVal = FLS_E_OK;
  }
  else //(HAL_ERROR == halRetVal)
  {
    retVal = FLS_E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           Wait for the last operation
 * @param[in]       uint32 Timeout value in ms
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_WaitForLastOperation(uint32 timoutMs)
{
  HAL_StatusTypeDef halRetVal;
  Fls_ReturnType retVal;

  halRetVal = FLASH_WaitForLastOperation(timoutMs);

  if(HAL_TIMEOUT == halRetVal)
  {
    retVal = FLS_E_PENDING;
  }
  else if(HAL_OK == halRetVal)
  {
    retVal = FLS_E_OK;
  }
  else
  {
    retVal = FLS_E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           Request actual flash memory status
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLS_E_OK
 *                  > FLS_E_NOT_OK
 *                  > FLS_E_PENDING
 * @note            Conversion of flash hw status register bits to Fls_ReturnType
 */
Fls_ReturnType Fls_GetFlsMemoryStatus( void )
{
  Fls_ReturnType retVal = FLS_E_OK;

  if(SET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP))
  {
    retVal = FLS_E_OK;
  }
  else if(SET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY))
  {
    retVal = FLS_E_PENDING;
  }
  else if(SET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_OPERR))
  {
    retVal = FLS_E_NOT_OK;
  }
  else if( (RESET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY))  &&  (RESET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP))  && (RESET == __HAL_FLASH_GET_FLAG(FLASH_FLAG_OPERR)) )
  {
    /* No operation started system ready for an operation*/
    retVal = FLS_E_OK;
  }
  else
  {
    #if(STD_ON == FLS_MODULE_USE_DET)
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    retVal = FLS_E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           Trigger a synchronous erase of an complete flash sector
 * @param[in]       uint32 Sector Id which shall be erased to be erased.
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncEraseFlsSector( uint32 sectorId )
{
  HAL_StatusTypeDef halRetVal;
  FLASH_EraseInitTypeDef flsEraseCfg;
  Fls_ReturnType retVal;
  boolean sectorIdValid = FALSE;
  uint32 idx, sectorError;

  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if(sectorId == flsSectorCfgList[idx].flsId)
    {
      sectorIdValid = TRUE;
      break;
    }
  }

  if(FALSE == sectorIdValid)
  {
    #if(STD_ON == FLS_MODULE_USE_DET)
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    flsEraseCfg.TypeErase = FLASH_TYPEERASE_SECTORS;
    flsEraseCfg.Banks = FLASH_BANK_1;
    flsEraseCfg.Sector = sectorId;
    flsEraseCfg.NbSectors = 1;
    flsEraseCfg.VoltageRange = FLS_USED_VOLTAGE_RANGE;

    /* Trigger sector erase */
    halRetVal = HAL_FLASHEx_Erase(&flsEraseCfg, &sectorError);

    /* Check return value */
    if(HAL_OK == halRetVal)
    {
      retVal = FLS_E_OK;
    }
    else if(HAL_ERROR == halRetVal)
    {
      retVal = FLS_E_NOT_OK;
    }
    else if(HAL_BUSY == halRetVal)  //DEBUG: NOTE: Maybe HAL dose not accept the request because the fls is busy so the correct return value would be skipped 
    {
      retVal = FLS_E_PENDING;
    }
    else //if(HAL_TIMEOUT == halRetVal)
    {
      retVal = FLS_E_PENDING;
    }
  }


  return retVal;
}

/**
 * @brief           Programming synchronous one byte to the flash
 * @param[in]       uint8 Byte data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramByte(uint8 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 1) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = fncRetVal;
  }
  else
  {
    /* Flash memory ready to be programmed >> Start byte programming */
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, address, (uint64) data);  /* HAL function allways return HAL_OK */
    retVal = Fls_GetFlsMemoryStatus();
  }

  return retVal;
}
/**
 * @brief           Programming synchronous one half word to the flash
 * @param[in]       uint16 half word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramHalfWord(uint16 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 2) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = fncRetVal;
  }
  else
  {
    /* Flash memory ready to be programmed >> Start byte programming */
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address, (uint64) data);  /* HAL function allways return HAL_OK */
    retVal = Fls_GetFlsMemoryStatus();
  }

  return retVal;
}

/**
 * @brief           Programming synchronous one word to the flash
 * @param[in]       uint32 word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramWord(uint32 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 4) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = fncRetVal;
  }
  else
  {
    /* Flash memory ready to be programmed >> Start byte programming */
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, (uint64) data);  /* HAL function allways return HAL_OK */
    retVal = Fls_GetFlsMemoryStatus();
  }

  return retVal;
}


/**
 * @brief           Programming synchronous one double word to the flash
 * @param[in]       uint64 double word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramDoubleWord(uint64 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 8) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }
  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = fncRetVal;
  }
  else
  {
    /* Flash memory ready to be programmed >> Start byte programming */
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address, (uint64) data);  /* HAL function allways return HAL_OK */
    retVal = Fls_GetFlsMemoryStatus();
  }

  return retVal;
}


/**
 * @brief           Trigger a asynchronous erase of an complete flash sector
 * @param[in]       uint32 Sector Id which shall be erased to be erased.
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncEraseFlsSector( uint32 sectorId )
{
  Fls_ReturnType retVal;
  boolean sectorIdValid = FALSE;
  uint32 idx;

  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if(sectorId == flsSectorCfgList[idx].flsId)
    {
      sectorIdValid = TRUE;
      break;
    }
  }

  if(FALSE == sectorIdValid)
  {
    #if(STD_ON == FLS_MODULE_USE_DET)
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    /* Trigger sector erase */
    FLASH_Erase_Sector(sectorId, FLS_USED_VOLTAGE_RANGE);
    retVal = FLS_E_OK;
  }
  return retVal;
}

/**
 * @brief           Programming asynchronous one byte to the flash
 * @param[in]       uint8 Byte data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramByte(uint8 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 1) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= FLASH_PSIZE_BYTE;
    FLASH->CR |= FLASH_CR_PG;
    *(volatile uint8*)address = data;

    retVal = FLS_E_OK;
  }

  return retVal;
}

/**
 * @brief           Programming asynchronous one half word to the flash
 * @param[in]       uint16 half word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramHalfWord(uint16 data, uint32 address)
{
  Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 2) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= FLASH_PSIZE_HALF_WORD;
    FLASH->CR |= FLASH_CR_PG;
    *(volatile uint16*)address = data;

    retVal = FLS_E_OK;
  }

  return retVal;
}

/**
 * @brief           Programming asynchronous one word to the flash
 * @param[in]       uint32 word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramWord(uint32 data, uint32 address)
{
Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 4) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= FLASH_PSIZE_WORD;
    FLASH->CR |= FLASH_CR_PG;
    *(volatile uint32*)address = data;

    retVal = FLS_E_OK;
  }

  return retVal;
}


/**
 * @brief           Programming asynchronous one double word to the flash
 * @param[in]       uint64 double word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_AsyncProgramDoubleWord(uint64 data, uint32 address)
{
Fls_ReturnType fncRetVal, retVal = FLS_E_NOT_OK;
  uint16 idx;
  boolean addressRangeFound = FALSE;

  #if(STD_ON == FLS_MODULE_USE_DET)
  /* Check input parameter */
  for(idx = 0; idx < FLS_AVAILABLE_FLS_SECTORS; idx++)
  {
    if( ( address >= flsSectorCfgList[idx].flsSectorStartAdr) && ( (address + 8) < flsSectorCfgList[idx].flsSectorEndAdr))
    {
      addressRangeFound = TRUE;
      break;
    }
  }

  if(addressRangeFound == FALSE)
  {
    Det_ReportError(FLS_DET_MODULE_ID, FLS_E_INVALID_ARGUMENT);
  }

  #endif

  /* Check flash memory status */
  fncRetVal = Fls_GetFlsMemoryStatus();
  if(fncRetVal != FLS_E_OK)
  {
    retVal = FLS_E_NOT_OK;
  }
  else
  {
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= FLASH_PSIZE_DOUBLE_WORD;
    FLASH->CR |= FLASH_CR_PG;

    /* Program the double-word */
    *(volatile uint64*)address = (uint64)data;

    retVal = FLS_E_OK;
  }

  return retVal;
}

#define Fls_STOP_SEC_CODE
#include "Fls_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */


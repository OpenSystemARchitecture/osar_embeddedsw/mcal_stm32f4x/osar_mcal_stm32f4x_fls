/****************************************************************************************************************************
* @file        Det.h                                                                                                        *
* @author      OSAR Team Reinemuth Sebastian                                                                                *
* @date        02-09-2017                                                                                                   *
* @brief       Implementation for Det Module Definitions                                                                    *
* @details     Description of Interfaces and global definitions                                                             *
* @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
*              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
*              License, or (at your option) any later version.                                                              *
*                                                                                                                           *
*              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
*              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
*              License for more details http://www.gnu.org/licenses/.                                                       *
* last checkin :                                                                                                            *
* $Author: sreinemuth $ @ $Date: 2018-08-04 14:26:58 +0200 (Sa, 04 Aug 2018) $ , Revision: $Rev: 390 $                       *
****************************************************************************************************************************/

/** @addtogroup  Det Det Definitions 
* @{
*/
#ifndef __DET_H
#define __DET_H

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>              Start of public include area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>               End of public include area                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
  * @fnmerge	  Std_ReturnType Det_Init(void);
  * @brief      Initialize the Det Module
  * @param      none
  * @retval     Std_ReturnType would always return E_OK
  */
Std_ReturnType Det_Init(void);

/**
  * @fnmerge	  Std_ReturnType Det_InitMemory(void);
  * @brief      Initialize the Det Module
  * @param      none
  * @retval     Std_ReturnType would always return E_OK
  */
Std_ReturnType Det_InitMemory(void);

/**
  * @fnmerge	  Std_ReturnType Det_ReportError(uint16 moduleId, uint16 errorId);
  * @brief      API to report an det error
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique 
  * @retval     Std_ReturnType would always return E_OK
  * @note       If the user dose not release the Det error it would never return
  */
Std_ReturnType Det_ReportError(uint16 moduleId, uint16 errorId);

/**
  * @fnmerge	  Std_ReturnType Det_AddUserDeviation(uint16 moduleId, uint16 errorId);
  * @brief      API to add an user deviation
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique
  * @retval     Std_ReturnType would return E_NOT_Ok if no more user deviations are available
  * @note       This function would report itself an det error
  */
Std_ReturnType Det_AddUserDeviation(uint16 moduleId, uint16 errorId);

/**
  * @fnmerge	  Std_ReturnType Det_RemoveUserDeviation(uint16 moduleId, uint16 errorId);
  * @brief      API to remove an user deviation
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique
  * @retval     Std_ReturnType would return E_NOT_OK if user deviation could not be found
  */
Std_ReturnType Det_RemoveUserDeviation(uint16 moduleId, uint16 errorId);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of public function prototypes                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#endif /* __DET_H */

/** @defgroup Det Development Error Trace Module
*/
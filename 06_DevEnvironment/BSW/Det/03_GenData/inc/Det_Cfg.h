/*****************************************************************************************************************************
 * @file        Det_Cfg.h                                                                                                    *
 * @author      OSAR DET Generator v.1.2.1.1                                                                                 *
 * @date        04.08.2018 14:25:05                                                                                          *
 * @brief       Implementation of DET module configuration.                                                                  *
 * @version     v.1.2.0                                                                                                      *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.3.3                                                                           *
*****************************************************************************************************************************/
#ifndef __DET_CFG_H
#define __DET_CFG_H
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available Errors within the Det Module
 */
typedef enum{
  DET_E_DET_DEVIATIONS_FULL = 0
}DET_E_EERROR;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define DET_MODULE_ID               0
#define DET_MODULE_ENABLED          STD_ON
#define DET_MODULE_USE_DET          STD_ON
#define DET_MAX_DET_USER_DEVIATIONS 10
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Enter Det_ReportError function.
 * @param[in]       Module Id
 * @param[in]       Error Id
 * @retval          None
 */
void Det_EnterReportErrorFnc( uint16 ModuleId, uint16 ErrorId );

/**
 * @brief           Leave Det_ReportError function.
 * @param[in]       Module Id
 * @param[in]       Error Id
 * @retval          None
 */
void Det_LeaveReportErrorFnc( uint16 ModuleId, uint16 ErrorId );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

#endif /* __DET_CFG_H*/

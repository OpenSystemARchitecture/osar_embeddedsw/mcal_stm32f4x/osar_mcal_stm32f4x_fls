/*****************************************************************************************************************************
 * @file        Std_Types.h                                                                                                  *
 * @author      OSAR Team Reinemuth Sebastian                                                                                *
 * @date        02-09-2017                                                                                                   *
 * @brief       Standard Definitions of all Datatypes used from all modules.                                                 *
 * @details     This file is used to redefine all cotoller/compiler specific data types to an standard data type             *
 * @version     1.0.4                                                                                                        *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 * last checkin :                                                                                                            *
 * $Author: sreinemuth $ @ $Date: 2018-08-04 14:26:58 +0200 (Sa, 04 Aug 2018) $ , Revision: $Rev: 390 $                       *
 ****************************************************************************************************************************/
/** @addtogroup General Standard definitions of data types
 * @{
 */
#ifndef __STD_TYPES_H
#define __STD_TYPES_H

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>              Start of public include area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>               End of public include area                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>              Start of USER include area                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
//#include "Compiler_Types.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>               End of USER include area                       << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*
 * @enum    boolean
 * @brief   Definition of generic bool
 */
typedef enum{
    FALSE = 0,      //!< Definition of FALSE
    TRUE            //!< Definition of TRUE
}boolean;

/*
 * @enum    Std_ReturnType
 * @brief   Definition of a Standard Return type
 */
typedef enum{
    E_OK = 0,               //!< Process end successful
    E_NOT_OK = 1,           //!< Process end not successful
    E_PENDING = 2,          //!< Process is pending
    E_NOT_IMPLEMENTED = 3   //!< Process is not implemented
}Std_ReturnType;

/*
* @enum    ModuleUsage
* @brief   Definition of a Module Usage 
*/
typedef enum{
  STD_OFF = 0,  //!< Standard definition to activate an functionality
  STD_ON = 1    //!< Standard definition to deactivate an functionality
}ModuleUsage;
#define STD_OFF 0
#define STD_ON 1
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define NULL                0           //!< Standard Definition for NULL as data value
#define NULL_PTR            (void*)0    //!< Standard Definition for a null pointer or an invalid pointer

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start standard type redefinition                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
typedef unsigned char       uint8;         //!< Standard Definition for uint8_t >> Range 8Bit >> 0 to 255
typedef char                sint8;         //!< Standard Definition for int8_t >> Range 8Bit >> -128 to 127

typedef unsigned short      uint16;        //!< Standard Definition for uint16_t  >> Range 16Bit >> 0 to 65.535
typedef short               sint16;        //!< Standard Definition for int16_t >> Range 16Bit >> -32.768 to 32.767

typedef unsigned int        uint32;        //!< Standard Definition for uint32_t  >> Range 32Bit >> 0 to 4.294.967.295
typedef int                 sint32;        //!< Standard Definition for int32_t >> Range 32Bit >> -2.147.483.648 to 2.147.483.647

typedef unsigned long long  uint64;        //!< Standard Definition for uint32_t  >> Range 64Bit >> 0 to 18.446.744.073.709.551.615
typedef long long           sint64;        //!< Standard Definition for int32_t >> Range 64Bit >> –9.223.372.036.854.775.808  to 9.223.372.036.854.775.807 

typedef float               float32;       //!< Standard Definition for float32_t  >> Range 32Bit >> 1.175494351e-38 to 3.40282347e+38
typedef double              float64;       //!< Standard Definition for float64_t  >> Range 64Bit >> 2.22507385850720138e-308 to 1.79769313486231571e+308

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End standard type redefinition                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#endif
/** @defgroup General Standard definitions of data types
 */
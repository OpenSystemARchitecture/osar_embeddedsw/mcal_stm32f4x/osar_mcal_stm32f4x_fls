/****************************************************************************************************************************
* @file        MemMap.h                                                                                                     *
* @author      OSAR Team Reinemuth Sebastian                                                                                *
* @date        02-09-2017                                                                                                   *
* @brief       Default implementation of MemMap.h                                                                           *
* @details     The user should adapt this file to implement his own memory mapping using compiler defines                   *
* @version     1.0.0                                                                                                        *
* @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
*              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
*              License, or (at your option) any later version.                                                              *
*                                                                                                                           *
*              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
*              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
*              License for more details http://www.gnu.org/licenses/.                                                       *
* last checkin :                                                                                                            *
* $Author: sreinemuth $ @ $Date: 2018-08-04 14:26:58 +0200 (Sa, 04 Aug 2018) $ , Revision: $Rev: 390 $                       *
*****************************************************************************************************************************/

#define MEMMAP

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             Start of Default Mapping sections                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Default mapping of code data -----------------------------------------------------*/
#ifdef START_DEFAULT_SECTION_CODE
#undef START_DEFAULT_SECTION_CODE
#ifdef START_DEFAULT_SECTION_CODE_START
#error Memmory Mapping Error detected! First close a section before reopen it!
#else
#define START_DEFAULT_SECTION_CODE_START
#endif
//#pragma section .text //Implement open compiler specific linker section

#undef MEMMAP
#endif

#ifdef STOP_DEFAULT_SECTION_CODE
#undef STOP_DEFAULT_SECTION_CODE
#ifndef START_DEFAULT_SECTION_CODE_START
#error Memmory Mapping Error detected! First open a section before close it!
#else
#undef START_DEFAULT_SECTION_CODE_START
#endif
//#pragma section	//Implement closing compiler specific linker section
#undef MEMMAP
#endif 

/* Default mapping of constant data -----------------------------------------------------*/
#ifdef START_DEFAULT_SECTION_CONST
#undef START_DEFAULT_SECTION_CONST
#ifdef START_DEFAULT_SECTION_CONST_START
#error Memmory Mapping Error detected! First close a section before reopen it!
#else
#define START_DEFAULT_SECTION_CONST_START
#endif
//#pragma section .text //Implement open compiler specific linker section

#undef MEMMAP
#endif

#ifdef STOP_DEFAULT_SECTION_CONST
#undef STOP_DEFAULT_SECTION_CONST
#ifndef START_DEFAULT_SECTION_CONST_START
#error Memmory Mapping Error detected! First open a section before close it!
#else
#undef START_DEFAULT_SECTION_CONST_START
#endif
//#pragma section	//Implement closing compiler specific linker section
#undef MEMMAP
#endif

/* Default mapping of uninitialized variables -----------------------------------------------------*/
#ifdef START_DEFAULT_SECTION_NOINIT_VAR
#undef START_DEFAULT_SECTION_NOINIT_VAR
#ifdef START_DEFAULT_SECTION_NOINIT_VAR_START
#error Memmory Mapping Error detected! First close a section before reopen it!
#else
#define START_DEFAULT_SECTION_NOINIT_VAR_START
#endif
//#pragma section .text //Implement open compiler specific linker section

#undef MEMMAP
#endif

#ifdef STOP_DEFAULT_SECTION_NOINIT_VAR
#undef STOP_DEFAULT_SECTION_NOINIT_VAR
#ifndef START_DEFAULT_SECTION_NOINIT_VAR_START
#error Memmory Mapping Error detected! First open a section before close it!
#else
#undef START_DEFAULT_SECTION_NOINIT_VAR_START
#endif
//#pragma section	//Implement closing compiler specific linker section
#undef MEMMAP
#endif 

/* Default mapping of initialized variables -----------------------------------------------------*/
#ifdef START_DEFAULT_SECTION_INIT_VAR
#undef START_DEFAULT_SECTION_INIT_VAR
#ifdef START_DEFAULT_SECTION_INIT_VAR_START
#error Memmory Mapping Error detected! First close a section before reopen it!
#else
#define START_DEFAULT_SECTION_INIT_VAR_START
#endif
//#pragma section .text //Implement open compiler specific linker section

#undef MEMMAP
#endif

#ifdef STOP_DEFAULT_SECTION_INIT_VAR
#undef STOP_DEFAULT_SECTION_INIT_VAR
#ifndef START_DEFAULT_SECTION_INIT_VAR_START
#error Memmory Mapping Error detected! First open a section before close it!
#else
#undef START_DEFAULT_SECTION_INIT_VAR_START
#endif
//#pragma section	//Implement closing compiler specific linker section
#undef MEMMAP
#endif 

/* Default mapping of zero initialized variables -----------------------------------------------------*/
#ifdef START_DEFAULT_SECTION_ZERO_INIT_VAR
#undef START_DEFAULT_SECTION_ZERO_INIT_VAR
#ifdef START_DEFAULT_SECTION_ZERO_INIT_VAR_START
#error Memmory Mapping Error detected! First close a section before reopen it!
#else
#define START_DEFAULT_SECTION_ZERO_INIT_VAR_START
#endif
//#pragma section .text //Implement open compiler specific linker section

#undef MEMMAP
#endif

#ifdef STOP_DEFAULT_SECTION_ZERO_INIT_VAR
#undef STOP_DEFAULT_SECTION_ZERO_INIT_VAR
#ifndef START_DEFAULT_SECTION_ZERO_INIT_VAR_START
#error Memmory Mapping Error detected! First open a section before close it!
#else
#undef START_DEFAULT_SECTION_ZERO_INIT_VAR_START
#endif
//#pragma section	//Implement closing compiler specific linker section
#undef MEMMAP
#endif 

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>               End of Default Mapping sections                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

#ifdef MEMMAP
#error Memmory Mapping Error detected in MemMap.h!
#undef MEMMAP
#endif
/*****************************************************************************************************************************
* @file        Rte_MemMap.h
* @author      OSAR Team S.Reinemuth
* @date        02.03.2019 08:07:55
* @brief       Implementation for MemoryMapping Definitions for Rte Module
* @details     This definitions are used to map the code and data within the Module
* @version     1.0.0
* @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    
*              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         
*              License, or (at your option) any later version. All code is distributed in the hope that it will be          
*              useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      
*              PARTICULAR PURPOSE. See the GNU General Public License for more details http://www.gnu.org/licenses/.        
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Start Memory Mapping                     << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_CODE       Rte_STOP_SEC_CODE                                                                               */
/****************************************************************************************************************************/
#ifdef Rte_START_SEC_CODE
#undef Rte_START_SEC_CODE
#define START_DEFAULT_SECTION_CODE
#include "MemMap.h"
#endif

#ifdef Rte_STOP_SEC_CODE
#undef Rte_STOP_SEC_CODE
#define STOP_DEFAULT_SECTION_CODE
#include "MemMap.h"
#endif
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     End Memory Mapping                       << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_CODE       Rte_STOP_SEC_CODE                                                                               */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Start Memory Mapping                     << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_CONST       Rte_STOP_SEC_NOINIT_CONST                                                                      */
/****************************************************************************************************************************/
#ifdef Rte_START_SEC_CONST
#undef Rte_START_SEC_CONST
#define START_DEFAULT_SECTION_CONST
#include "MemMap.h"
#endif

#ifdef Rte_STOP_SEC_CONST
#undef Rte_STOP_SEC_CONST
#define STOP_DEFAULT_SECTION_CONST
#include "MemMap.h"
#endif
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     End Memory Mapping                       << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_CONST       Rte_STOP_SEC_NOINIT_CONST                                                                      */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Start Memory Mapping                     << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_NOINIT_VAR       Rte_STOP_SEC_NOINIT_VAR                                                                   */
/****************************************************************************************************************************/
#ifdef Rte_START_SEC_NOINIT_VAR
#undef Rte_START_SEC_NOINIT_VAR
#define START_DEFAULT_SECTION_NOINIT_VAR
#include "MemMap.h"
#endif

#ifdef Rte_STOP_SEC_NOINIT_VAR
#undef Rte_STOP_SEC_NOINIT_VAR
#define STOP_DEFAULT_SECTION_NOINIT_VAR
#include "MemMap.h"
#endif
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     End Memory Mapping                       << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_NOINIT_VAR       Rte_STOP_SEC_NOINIT_VAR                                                                   */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Start Memory Mapping                     << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_INIT_VAR       Rte_STOP_SEC_INIT_VAR                                                                       */
/****************************************************************************************************************************/
#ifdef Rte_START_SEC_INIT_VAR
#undef Rte_START_SEC_INIT_VAR
#define START_DEFAULT_SECTION_INIT_VAR
#include "MemMap.h"
#endif

#ifdef Rte_STOP_SEC_INIT_VAR
#undef Rte_STOP_SEC_INIT_VAR
#define STOP_DEFAULT_SECTION_INIT_VAR
#include "MemMap.h"
#endif
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     End Memory Mapping                       << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_INIT_VAR       Rte_STOP_SEC_INIT_VAR                                                                       */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Start Memory Mapping                     << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_INIT_VAR       Rte_STOP_SEC_INIT_VAR                                                                       */
/****************************************************************************************************************************/
#ifdef Rte_START_SEC_ZERO_INIT_VAR
#undef Rte_START_SEC_ZERO_INIT_VAR
#define START_DEFAULT_SECTION_ZERO_INIT_VAR
#include "MemMap.h"
#endif

#ifdef Rte_STOP_SEC_ZERO_INIT_VAR
#undef Rte_STOP_SEC_ZERO_INIT_VAR
#define STOP_DEFAULT_SECTION_ZERO_INIT_VAR
#include "MemMap.h"
#endif
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     End Memory Mapping                       << DO NOT CHANGE THIS COMMENT */
/* Rte_START_SEC_INIT_VAR       Rte_STOP_SEC_INIT_VAR                                                                       */
/****************************************************************************************************************************/


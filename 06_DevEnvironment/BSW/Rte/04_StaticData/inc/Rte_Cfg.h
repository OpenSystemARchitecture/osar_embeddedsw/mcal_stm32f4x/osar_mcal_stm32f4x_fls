/*****************************************************************************************************************************
 * @file        Rte_Cfg.h                                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.03.2019 08:07:55                                                                                          *
 * @brief       Generic configuration parameters of the Rte module.                                                          *
 * @details     Definition of general configuration parameters and values.                                                   *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.1.5                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_CFG_H
#define __RTE_CFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------  Development error trace information for this module ----------------------------------*/
#define RTE_DET_MODULE_ID       10
#define RTE_MODULE_USE_DET      STD_ON

/*--------------------------------------  Definiton of data access type abstractions  --------------------------------------*/
#define VAR( x )                x                 /*!< Direct variable access */
#define P2VAR( x )              x *               /*!< Pointer variable >> Full data access */
#define P2CONSTVAR( x )         const x *         /*!< Pointer to const variable >> Pointer could be modified >> Variable is constant */
#define CONSTP2VAR( x )         x * const         /*!< Const pointer to variable >> Pointer could not be modified >> Variable could be modified */
#define CONSTP2CONSTVAR( x )    const x * const   /*!< Const pointer to const variable >> Pointer and variable could not be modified */

/*--------------------------------------------  Rte default return type values   -------------------------------------------*/
#define RTE_E_OK                            0     /*!< Rte default return value for operation succeeded */
#define RTE_E_NOT_OK                        1     /*!< Rte default not ok return value */
#define RTE_E_PENDING                       2     /*!< Rte default operation pending return value */
#define RTE_E_NOT_IMPLEMENTED               3     /*!< Rte default operation not implemented return value */
#define RTE_E_GENERIC_PROGRAMMING_FAILURE   4     /*!< Rte default generic programming failure return value */
#define RTE_E_NOT_CONNECTED                 5     /*!< Rte default port not connected return value */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_CFG_H*/

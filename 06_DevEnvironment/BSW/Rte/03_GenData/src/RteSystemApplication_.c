/*****************************************************************************************************************************
 * @file        RteSystemApplication_.c                                                                                      *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        09.12.2020 15:59:42                                                                                          *
 * @brief       Generated Rte System Application Source File: RteSystemApplication_.c                                        *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "RteSystemApplication_.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------------- Map functions into OSAR Code sections -----------------------------------------*/
#define Rte_START_SEC_CODE
#include "Rte_MemMap.h"
/**
 * @brief           Rte generated system application init task.
 */
VAR(void) RteSystemApplicationInitTask_( VAR(void) )
{

}

/**
 * @brief           Rte generated system application idle task.
 */
VAR(void) RteSystemApplicationIdleTask_( VAR(void) )
{

}

/**
 * @brief           Rte generated system application low priority (1) task.
 * @details         Generated task scheduling: 10000ms < runnable < ---
 *                  Scheduling time: 1000ms
 */
VAR(void) RteSystemApplicationPrio1Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

/**
 * @brief           Rte generated system application below normal priority (2) task.
 * @details         Generated task scheduling: 1000ms < runnable < 10000ms
 *                  Scheduling time: 100ms
 */
VAR(void) RteSystemApplicationPrio2Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

/**
 * @brief           Rte generated system application normal priority (3) task.
 * @details         Generated task scheduling: 500ms < runnable < 1000ms
 *                  Scheduling time: 10ms
 */
VAR(void) RteSystemApplicationPrio3Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

/**
 * @brief           Rte generated system application above normal priority (4) task.
 * @details         Generated task scheduling: 100ms < runnable < 500ms
 *                  Scheduling time: 10ms
 */
VAR(void) RteSystemApplicationPrio4Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

/**
 * @brief           Rte generated system application high priority (5) task.
 * @details         Generated task scheduling: 10ms < runnable < 100ms
 *                  Scheduling time: 1ms
 */
VAR(void) RteSystemApplicationPrio5Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

/**
 * @brief           Rte generated system application ultra high priority (6) task.
 * @details         Generated task scheduling: 1ms < runnable < 10ms
 *                  Scheduling time: 1ms
 */
VAR(void) RteSystemApplicationPrio6Task_( VAR(void) )
{
  static uint64 cnt = 0;

  cnt++;

}

#define Rte_STOP_SEC_CODE
#include "Rte_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */


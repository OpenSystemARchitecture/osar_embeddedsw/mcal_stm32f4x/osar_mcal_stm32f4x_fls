/*****************************************************************************************************************************
 * @file        Rte_Uart.h                                                                                                   *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        09.12.2020 15:59:42                                                                                          *
 * @brief       Generated Rte Module Interface Header File: Rte_Uart.h                                                       *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_UART_H
#define __RTE_UART_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Interfaces.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++++++++++ Generation of Init Runnable Prototypes ++++++++++++++++++++++++++++++++++++++++ */
VAR(void) Uart_Init( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++++ Generation of Cyclic Runnable Prototypes +++++++++++++++++++++++++++++++++++++++ */
VAR(void) Uart_Mainfunction( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++ Generation of Server Port Runnable Prototypes ++++++++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of Client Port Runnable Prototype Information ++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++ Generation of Sender Receiver Port Runnable Prototype Information ++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_UART_H*/

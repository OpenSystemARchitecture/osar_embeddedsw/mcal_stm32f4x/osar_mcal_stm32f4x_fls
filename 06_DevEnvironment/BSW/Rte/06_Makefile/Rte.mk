###########################################################################################################
# @brief    Module Makefile which references all include paths and source files for this specific module. #
###########################################################################################################

###########################################################################################################
########################################### ADDITIONAL_INCLUDES ###########################################
###########################################################################################################
ADDITIONAL_INCLUDES += -IBSW/Rte/02_Mandatory/inc
ADDITIONAL_INCLUDES += -IBSW/Rte/03_GenData/inc
ADDITIONAL_INCLUDES += -IBSW/Rte/04_StaticData/inc

###########################################################################################################
########################################### ADDITIONAL_SOURCES ############################################
###########################################################################################################
ADDITIONAL_SOURCES += BSW/Rte/04_StaticData/src/Rte.c
ADDITIONAL_SOURCES += BSW/Rte/03_GenData/src/Rte_Callout_Stubs.c
ADDITIONAL_SOURCES += BSW/Rte/03_GenData/src/Rte_Interfaces.c
ADDITIONAL_SOURCES += BSW/Rte/03_GenData/src/RteSystemApplication_Internal.c
ADDITIONAL_SOURCES += BSW/Rte/03_GenData/src/RteSystemApplication_.c

###########################################################################################################
############################################# ADDITIONAL_ASM ##############################################
###########################################################################################################
#ADDITIONAL_ASM += BSW/Rte/... 

###########################################################################################################
########################################### ADDITIONAL_DEFINES ############################################
###########################################################################################################
#ADDITIONAL_DEFINES += ... 
